const fallback = {
  id: 1,
  title: "Leider sind keine Post vorhanden",
  description:
    "Wenn du trozdem mehr erfahren möchtes, schaud dich auf der Seite um, oder besuch uns auf unseren Sozialen Medien. Die Links findest du unter diesem Post",
};

export default fallback;
