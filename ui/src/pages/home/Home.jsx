import React from "react";
import { toast } from "react-toastify";
import { loader } from "graphql.macro";
import { useQuery } from "@apollo/client";
import CircularProgress from "@material-ui/core/CircularProgress";

// Componets
import FlexColums from "../../components/flexColums/FlexColums";
import SocialMedia from "../../components/socialMedia/SocialMedia";

// Fallback
import Fallback from "./fallback";

// Stylesheet
import "./Home.scss";

// Import Fetch Schema
const FEED = loader("../../graphql/post/FEED.gql");

// Function Page Home
const Home = () => {
  // Fetch data
  const { loading, error, data } = useQuery(FEED, {
    fetchPolicy: "cache-and-network",
  });

  if (loading)
    return (
      <div className="center">
        <CircularProgress color="inherit" />
      </div>
    );
  if (error) {
    // toast.error(error.message, {
    //   position: "bottom-center",
    // });
    console.error(error.message);
  }
  if (data.error) {
    // toast.error(data.error.message, {
    //   position: "bottom-center",
    // });
    console.error(data.error.message);
  }

  let { posts } = data.feed;
  posts = posts.slice().reverse();

  return posts.length >= 1 ? (
    <main id="home">
      {/* New Topical */}
      <FlexColums key={posts[0].id} data={posts[0]} btn={true} />

      {/* Social Media Component */}
      <SocialMedia />

      {/* Older Topical */}
      {posts.slice(1, 5).map(o => (
        <FlexColums key={o.id} data={o} reverse={true} btn={true} />
      ))}
    </main>
  ) : (
    <main id="home">
      {/* Fallback */}
      <FlexColums key="fallback" data={Fallback} fallback={true} btn={true} />

      {/* Social Media Component */}
      <SocialMedia />
    </main>
  );
};

export default Home;
