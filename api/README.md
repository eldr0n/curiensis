# AV Curiensis POC Backend API
## Prisma usage
If using npm instead of yarn replace yarn with npx.
### migrate
```sh
yarn prisma migrate save --experimental
yarn prisma migrate up --experimental
```

### generate client 
```sh
yarn prisma generate
```
### prisma studio
```sh
yarn prisma studio --experimental
```