const express = require("express");
const path = require("path");
const { ApolloServer } = require("apollo-server-express");
const { PrismaClient } = require("@prisma/client");
const { importSchema } = require("graphql-import");
const typeDefs = importSchema("./src/schema.graphql");
const { feed } = require("./resolvers/queries/PostQuery");
const {
  user,
  users,
  committees,
  quotes,
} = require("./resolvers/queries/UserQuery");
const { events } = require("./resolvers/queries/EventQuery");
const { drinks } = require("./resolvers/queries/DrinkQuery");
const {
  addUser,
  editUser,
  removeUser,
} = require("./resolvers/mutations/UserMutation");
const {
  addPost,
  editPost,
  removePost,
} = require("./resolvers/mutations/PostMutation");
const { login, logout } = require("./resolvers/mutations/AuthMutation");
const {
  addEvent,
  editEvent,
  removeEvent,
} = require("./resolvers/mutations/EventMutation");
const {
  addDrink,
  removeDrink,
} = require("./resolvers/mutations/DrinkMutation");
const { uploadFile } = require("./resolvers/mutations/FileMutation");
const User = require("./resolvers/User");
const Post = require("./resolvers/Post");
const Drink = require("./resolvers/Drink");
const Event = require("./resolvers/Event");
const Quote = require("./resolvers/Quote");
const Committee = require("./resolvers/Committee");

const prisma = new PrismaClient();

const resolvers = {
  Query: { feed, user, users, committees, quotes, events, drinks },
  Mutation: {
    addUser,
    editUser,
    removeUser,
    addPost,
    editPost,
    removePost,
    login,
    logout,
    addEvent,
    editEvent,
    removeEvent,
    addDrink,
    removeDrink,
    uploadFile,
  },
  User,
  Post,
  Drink,
  Quote,
  Committee,
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: request => {
    return {
      ...request,
      prisma,
    };
  },
  // cors: {
  //   credentials: true,
  //   origin: "http://localhost:3000",
  //   preflightContinue: false,
  // },
  playgrpund: {
    endpoint: "/graphql",
    settings: {
      "request.credentials": "include",
    },
  },
});

const app = express();

app.use("/images", express.static(path.join(__dirname, "../images")));
app.use("/pdf", express.static(path.join(__dirname, "../pdf")));

server.applyMiddleware({ app, cors: { credentials: true, origin: true } });

app.listen({ port: 4000 }, () =>
  console.log(`🚀 Server is running on http://localhost:4000/graphql`)
);
