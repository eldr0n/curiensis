async function events(parent, args, context, info) {
  return await context.prisma.event.findMany();
}

module.exports = {
  events,
};
