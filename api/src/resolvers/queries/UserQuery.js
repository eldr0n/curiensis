const { getUserId, isAdmin } = require("../../utils/auth");

async function user(parent, args, context, info) {
  let userId = getUserId(context);
  if (isAdmin(context) && args.id) {
    userId = args.id
  }
  return await context.prisma.user.findOne({ where: { id: userId } });
}

async function users(parent, args, context, info) {
  getUserId(context);
  return await context.prisma.user.findMany();
}

async function committees(parent, args, context, info) {
  return await context.prisma.user.findMany({ where: { rank: { not: null } } });
}

async function quotes(parent, args, context, info) {
  return await context.prisma.user.findMany({ where: { quote: { not: null } } });
}

module.exports = {
  user,
  users,
  committees,
  quotes
};
