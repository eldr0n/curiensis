const { getUserId } = require("../../utils/auth");

async function drinks(parent, args, context, info) {
  const userId = getUserId(context);
  return await context.prisma.drink.findMany({
    where: { drinkedById: userId },
  });
}

module.exports = {
  drinks,
};
