function drinkedBy(parent, args, context) {
  return context.prisma.drink.findOne({ where: { id: parent.id } }).drinkedBy();
}

module.exports = {
  drinkedBy
};

