function postedBy(parent, args, context) {
  return context.prisma.post.findOne({ where: { id: parent.id } }).postedBy();
}

function image(parent, args, context) {
  return context.prisma.post.findOne({ where: { id: parent.id } }).image();
}

module.exports = {
  postedBy,
  image
};

