function image(parent, args, context) {
  return context.prisma.user.findOne({ where: { id: parent.id } }).image();
}

module.exports = {
  image
};