const bcrypt = require('bcryptjs');
const jwt = require("jsonwebtoken");
const { SECRET, getUserId } = require('../../utils/auth');

async function login(parent, args, context, info) {
  const user = await context.prisma.user.findOne({
    where: { email: args.email },
  });
  if (!user) {
    throw new Error('User not found');
  }

  const valid = await bcrypt.compare(args.password, user.password);
  if (!valid) {
    throw new Error('Invalid password');
  }

  const token = jwt.sign({ userId: user.id, role: user.role }, SECRET, {
    expiresIn: '1h',
  });

  const options = {
    maxAge: 1000 * 60 * 60,
    httpOnly: true,
    secure: true,
    sameSite: true,
  };

  context.res.cookie('token', token, options);

  return user;
}

async function logout(parent, args, context, info) {
  const options = {
    maxAge: -1000 * 60 * 60,
    httpOnly: true,
    secure: true,
    sameSite: true,
  };
  context.res.cookie('token', '', options);
  return true;
}

module.exports = {
  login,
  logout,
};
