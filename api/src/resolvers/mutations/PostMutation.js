const { getUserId, isAdmin } = require("../../utils/auth");
const { uploadFile, delteFile } = require("../../utils/fileHandler");

async function addPost(parent, args, context, info) {
  isAdmin(context);
  const userId = getUserId(context);
  const newPost = await context.prisma.post.create({
    data: { ...args, postedBy: { connect: { id: userId } } },
  });

  // if theres an image in args create the file
  // else create an empty one an reference to user
  if (args.image) {
    const image = await uploadFile(args.image);
    await context.prisma.file.create({
      data: {
        ...image,
        path: image.path.split("../../")[1],
        parentPost: { connect: { id: newPost.id } },
      },
    });
  } else {
    await context.prisma.file.create({
      data: {
        parentPost: { connect: { id: newPost.id } },
      },
    });
  }

  return newPost;
}

async function editPost(parent, args, context, info) {
  isAdmin(context);

  const editPost = await context.prisma.post.update({
    where: { id: args.id },
    data: {
      title: args.title,
      description: args.description,
    },
  });

  // if theres an image in args update the file
  if (args.image) {
    // get the old image
    const image = await context.prisma.file.findOne({
      where: { postById: args.id },
    });
    // delete the old image in fs
    if (image.path != null) {
      delteFile(image.path);
    }
    // create the new image and update in db
    const newImage = await uploadFile(args.image);
    await context.prisma.file.update({
      where: { id: image.id },
      data: {
        ...newImage,
        path: newImage.path.split("../../")[1],
        parentPost: { connect: { id: args.id } },
      },
    });
  }
  return editPost;
}

async function removePost(parent, args, context, info) {
  isAdmin(context);
  // get the referenced image
  const image = await context.prisma.file.findOne({
    where: { postById: args.id },
  });
  // delete image in fs
  if (image.path != null && image.path != "images/blank-profile-picture.png") {
    delteFile(image.path);
  }
  // delete image in db
  await context.prisma.file.delete({
    where: { postById: args.id },
  });
  // delete psot in db
  const rmPost = context.prisma.post.delete({
    where: { id: args.id },
  });
  return rmPost;
}

module.exports = {
  addPost,
  editPost,
  removePost,
};
