const { isAdmin, getUserId } = require("../../utils/auth");

async function addEvent(parent, args, context, info) {
  isAdmin(context);
  let time;
  if (args.time) {
    time = parseTime(args.time);
  }
  const userId = getUserId(context);
  const newEvent = await context.prisma.event.create({
    data: {
      title: args.title,
      location: args.location,
      city: args.city,
      time: time,
      type: args.type,
      outfit: args.outfit,
      eventBy: { connect: { id: userId } },
    },
  });

  return newEvent;
}

async function editEvent(parent, args, context, info) {
  isAdmin(context);

  let time;
  if (args.time) {
    time = parseTime(args.time);
  }

  const editEvent = await context.prisma.event.update({
    where: { id: args.id },
    data: {
      title: args.title,
      location: args.location,
      city: args.city,
      time: time,
      type: args.type,
      outfit: args.outfit,
    },
  });

  return editEvent;
}

async function removeEvent(parent, args, context, info) {
  isAdmin(context);
  const rmEvent = context.prisma.event.delete({
    where: { id: args.id },
  });
  return rmEvent;
}

function parseTime(timeString) {
  const time = timeString.split("-");
  return new Date(
    parseInt(time[0]),
    parseInt(time[1] - 1),
    parseInt(time[2]),
    parseInt(time[3]),
    parseInt(time[4])
  );
}

module.exports = {
  addEvent,
  editEvent,
  removeEvent,
};
