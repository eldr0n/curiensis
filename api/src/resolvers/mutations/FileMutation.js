const shortid = require("shortid");
const { createWriteStream, mkdir } = require("fs");

const storeUpload = async ({ stream, filename, mimetype }) => {
  const id = shortid.generate();
  const path = `images/${id}-${filename}`;
  // writes the file to the images directory
  return new Promise((resolve, reject) =>
    stream
      .pipe(createWriteStream(path))
      .on("finish", () => resolve({ id, path, filename, mimetype }))
      .on("error", reject)
  );
};

const processUpload = async (upload) => {
  const { createReadStream, filename, mimetype } = await upload;
  const stream = createReadStream();
  const file = await storeUpload({ stream, filename, mimetype });
  return file;
};

async function uploadFile(_, { file }) {
  mkdir("images", { recursive: true }, err => {
    if (err) throw err;
  });
  // Process upload
  const upload = await processUpload(file);
  return upload;
}

module.exports = {
  uploadFile,
};
