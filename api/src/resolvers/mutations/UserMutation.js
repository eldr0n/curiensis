const bcrypt = require("bcryptjs");
const { getUserId, isAdmin } = require("../../utils/auth");
const { uploadFile, delteFile } = require("../../utils/fileHandler");

async function addUser(parent, args, context, info) {
  isAdmin(context);
  const password = await bcrypt.hash(args.password, 10);
  const newUser = await context.prisma.user.create({
    data: { ...args, password },
  });

  // if theres an image in args create the file
  // else create an empty one an reference to user
  if (args.image) {
    const image = await uploadFile(args.image);
    await context.prisma.file.create({
      data: {
        ...image,
        path: image.path.split("../../")[1],
        parentUser: { connect: { id: newUser.id } },
      },
    });
  } else {
    await context.prisma.file.create({
      data: {
        parentUser: { connect: { id: newUser.id } },
      },
    });
  }

  return newUser;
}

async function editUser(parent, args, context, info) {
  let userId = getUserId(context);
  // orher users, rank and role can only be edited by an admin
  if (args.id || args.rank || args.role) {
    isAdmin(context);
    userId = args.id;
  }
  let editUser;

  // if theres a pw in args update it else skip pw
  if (args.password) {
    const newPassword = await bcrypt.hash(args.password, 10);
    editUser = await context.prisma.user.update({
      where: { id: userId },
      data: { ...args, password: newPassword },
    });
  } else {
    editUser = await context.prisma.user.update({
      where: { id: userId },
      data: { ...args },
    });
  }

  // if theres an image in args update the file
  if (args.image) {
    // get the old image
    const image = await context.prisma.file.findOne({
      where: { userById: userId },
    });

    // delete the old image in fs
    if (
      image.path != null &&
      image.path != "images/blank-profile-picture.png"
    ) {
      delteFile(image.path);
    }

    // create the new image and update it in db
    const newImage = await uploadFile(args.image);
    await context.prisma.file.update({
      where: { id: image.id },
      data: {
        ...newImage,
        path: newImage.path.split("../../")[1],
        parentUser: { connect: { id: userId } },
      },
    });
  }

  return editUser;
}

async function removeUser(parent, args, context, info) {
  let userId = getUserId(context);
  if (args.id) {
    isAdmin(context);
    userId = args.id;
  }
  // get the referenced image
  const image = await context.prisma.file.findOne({
    where: { userById: userId },
  });
  // delete image in fs
  if (image.path != null) {
    delteFile(image.path);
  }
  // delete image in db
  await context.prisma.file.delete({
    where: { userById: userId },
  });

  // delete user in db
  const rmUser = await context.prisma.user.delete({
    where: { id: userId },
  });

  return rmUser;
}

module.exports = {
  addUser,
  editUser,
  removeUser,
};
