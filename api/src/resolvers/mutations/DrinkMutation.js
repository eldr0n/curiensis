const { getUserId } = require("../../utils/auth");

async function addDrink(parent, args, context, info) {
  const userId = getUserId(context);
  const newDrink = await context.prisma.drink.create({
    data: { ...args, drinkedBy: { connect: { id: userId } } },
  });
  return newDrink;
}

async function removeDrink(parent, args, context, info) {
  const userId = getUserId(context);
  const rmDrink = await context.prisma.drink.deleteMany({
    where: { id: args.id, drinkedById: userId },
  });
  console.log(rmDrink)
  return rmDrink;
}

module.exports = {
  addDrink,
  removeDrink,
};
