function events(parent, args, context) {
  return context.prisma.event.findOne({ where: { id: parent.id } }).events();
}

module.exports = {
  events
};
