function posts(parent, args, context) {
  return context.prisma.user.findOne({ where: { id: parent.id } }).posts();
}

function drinks(parent, args, context) {
  return context.prisma.user.findOne({ where: { id: parent.id } }).drinks();
}

function image(parent, args, context) {
  return context.prisma.user.findOne({ where: { id: parent.id } }).image();
}

module.exports = {
  posts,
  drinks,
  image
};
