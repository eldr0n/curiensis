const shortid = require("shortid");
const { createWriteStream, mkdir, unlink } = require("fs");

const storeUpload = async ({ stream, filename, mimetype }) => {
  const path = `${__dirname}/../../images/${shortid.generate()}-${filename}`;
  // writes the file to the images directory
  return new Promise((resolve, reject) =>
    stream
      .pipe(createWriteStream(path))
      .on("finish", () => resolve({ path, filename, mimetype }))
      .on("error", reject)
  );
};

const processUpload = async upload => {
  const { createReadStream, filename, mimetype } = await upload;
  const stream = createReadStream();
  const file = await storeUpload({ stream, filename, mimetype });
  return file;
};

async function uploadFile(file) {
  mkdir(`${__dirname}/../../images`, { recursive: true }, err => {
    if (err) throw err;
  });
  // Process upload
  const upload = await processUpload(file);
  return upload;
}

function delteFile(path) {
  unlink(`${__dirname}/../../${path}`, err => {
    if (err) {
      console.log(err);;
    }
  });
}

module.exports = {
  uploadFile,
  delteFile,
};
