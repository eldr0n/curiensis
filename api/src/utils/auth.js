const jwt = require("jsonwebtoken");
const SECRET = process.env.SECRET || "notsosecret";

function getUserId(context) {
  const cookie = context.req.headers.cookie;
  if (cookie) {
    const token = cookie.replace("token=", "");
    const { userId } = jwt.verify(token, SECRET);
    return userId;
  }

  throw new Error("Unauthorized");
}

function isAdmin(context) {
  const cookie = context.req.headers.cookie;
  if (cookie) {
    const token = cookie.replace("token=", "");
    const { role } = jwt.verify(token, SECRET);
    return role == "ADMIN" ? true : false;
  }
}

module.exports = {
  SECRET,
  getUserId,
  isAdmin,
};
