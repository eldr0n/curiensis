# Migration `20201201090657`

This migration has been generated by Rico Good at 12/1/2020, 10:06:57 AM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
CREATE TABLE `event` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(191) NOT NULL,
    `location` VARCHAR(191) NOT NULL,
    `time` DATETIME(3) NOT NULL,
    `type` VARCHAR(191) NOT NULL,
    `outfit` VARCHAR(191) NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration 20201201084753-comment----quote..20201201090657
--- datamodel.dml
+++ datamodel.dml
@@ -1,7 +1,7 @@
 datasource db {
     provider = "mysql"
-    url = "***"
+    url = "***"
 }
 generator client {
     provider = "prisma-client-js"
@@ -63,8 +63,17 @@
     parentUser User?   @relation(fields: [userById], references: [id])
     userById   String? @unique
 }
+model event {
+    id  Int @id @default(autoincrement())
+    title String
+    location String
+    time DateTime 
+    type String
+    outfit String
+}
+
 enum Beverage {
     BEER
     WINE
     CIDER
```


