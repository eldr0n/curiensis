# Migration `20201203120130-add-ah-and-aktivitas-enums`

This migration has been generated by Rico Good at 12/3/2020, 1:01:30 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
ALTER TABLE `User` MODIFY `rank` ENUM('SENIOR', 'COSENIOR', 'FM', 'QUESTOR', 'AKTUAR', 'REVISOR', 'AHP', 'AHKASSIER', 'AH', 'AKTIVITAS')
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration 20201201091137-add-city-to-event..20201203120130-add-ah-and-aktivitas-enums
--- datamodel.dml
+++ datamodel.dml
@@ -1,7 +1,7 @@
 datasource db {
     provider = "mysql"
-    url = "***"
+    url = "***"
 }
 generator client {
     provider = "prisma-client-js"
@@ -93,8 +93,10 @@
     AKTUAR
     REVISOR
     AHP
     AHKASSIER
+    AH
+    AKTIVITAS
 }
 enum Role {
     USER
```


