const { PrismaClient } = require('@prisma/client');
const prisma = new PrismaClient();
const bcrypt = require('bcryptjs');


// ALTER TABLE `Post` MODIFY `description` TEXT NOT NULL;
// ALTER TABLE User ADD CONSTRAINT checkmail CHECK(email LIKE '%_@__%.__%');

async function addPost() {
  const newPost = await prisma.post.create({
    data: {
      title: 'Fuxenreise 2020',
      description:
        'Am Wochenende vom 10. und 11. Oktober führt uns die diesjährige Fuxenreise ins wildromantische Calfeisental.\nBei hoffentlich bestem Herbstwetter werden wir ein tolles Wochenende in dieser einmaligen Umgebung, mitten in der Tektonikarena Sardona verbringen.',
    },
  });
  const allPosts = await prisma.post.findMany();
  console.log(allPosts);
}

async function addUser() {
  //TODO: PW
  const password = await bcrypt.hash("", 10);
  const newUser = await prisma.user.create({
    data: {
      firstname: 'Hampi',
      lastname: 'Hobler',
      address: 'Gasse',
      city: 'Hogwarts',
      zip: 12,
      vulgo: 'Fumcart',
      email: 'hampi@hobler.io',
      role: "ADMIN",
      
      password: password,
    },
  });

  const allUsers = await prisma.user.findMany();
  console.log(allUsers);
}

addUser()
  .catch(e => {
    throw e;
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
// addPost()
//   .catch(e => {
//     throw e;
//   })
//   .finally(async () => {
//     await prisma.$disconnect();
//   });
